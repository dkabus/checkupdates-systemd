checkupdates-systemd
====================

This repository contains `systemd` unit files for the
`checkupdates` script in the `pacman-contrib` package.

* `service`: check for updates and download packages to cache
* `timer`:   run the service daily
* `hook`:    run the service after installing/upgrading packages with pacman

Installation
------------

To install straight from this repository, run:

	sudo make install

There is also an AUR package available: `checkupdates-systemd-git`.

Usage
-----

Activate the timer:

	sudo systemctl enable checkupdates.timer
