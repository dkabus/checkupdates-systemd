DIR=$(DESTDIR)$(or $(SYSTEMD),/usr/lib/systemd/system/)$(or $(NAME),checkupdates)

all: updates

updates:
	touch $@

install: updates
	install -Dm644 service "$(DIR).service"
	install -Dm644 timer "$(DIR).timer"
	install -Dm644 LICENSE -t "$(DESTDIR)/usr/share/licenses/checkupdates-systemd/"
	install -Dm644 updates -t "$(DESTDIR)/usr/share/checkupdates-systemd/"
	install -Dm644 hook "$(DESTDIR)/usr/share/libalpm/hooks/checkupdates.hook"

uninstall:
	rm -f "$(DIR).service"
	rm -f "$(DIR).timer"
	rm -rf "$(DESTDIR)/usr/share/licenses/checkupdates-systemd/"
	rm -rf "$(DESTDIR)/usr/share/checkupdates-systemd/"
	rm -f "$(DESTDIR)/usr/share/libalpm/hooks/checkupdates.hook"

.PHONY: all install uninstall
